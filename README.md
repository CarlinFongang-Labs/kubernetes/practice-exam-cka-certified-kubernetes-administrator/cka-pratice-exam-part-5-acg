# 5. CKA Pratice Exam Part 5 ACG : Mise à Niveau des Composants Kubernetes

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. Mettre à niveau tous les composants Kubernetes sur le nœud du plan de contrôle

2. Mettre à niveau tous les composants Kubernetes sur les nœuds de travail

# Contexte

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le terrain réel. Examen CKA. Bonne chance!

Cette question utilise le acgk8scluster. Après vous être connecté au serveur d'examen, passez au contexte correct avec la commande kubectl config use-context acgk8s.

Chacun des objectifs représente une tâche que vous devrez accomplir en utilisant le cluster et les serveurs disponibles. Lisez attentivement chaque objectif et accomplissez la tâche spécifiée.

Pour certains objectifs, vous devrez peut-être vous connecter à d'autres nœuds ou serveurs à partir du serveur d'examen. Vous pouvez le faire en utilisant le nom d'hôte/nom de nœud (c'est-à-dire ssh acgk8s-worker1).

Remarque : Vous ne pouvez pas vous connecter à un autre nœud, ni l'utiliser kubectlpour vous connecter au cluster, à partir d'un nœud autre que le nœud racine. Une fois que vous avez terminé les tâches nécessaires sur un serveur, assurez-vous de quitter et de revenir au nœud racine avant de continuer.

Si vous devez assumer les privilèges root sur un serveur, vous pouvez le faire avec sudo -i.

Vous pouvez exécuter le script de vérification situé à `/home/cloud_user/verify.shtout` moment pour vérifier votre travail !

>![Alt text](img/image.png)

# Application

## Étape 1 : Connexion au Serveur

1. Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Mettre à Niveau Tous les Composants Kubernetes sur le Nœud du Plan de Contrôle

1. Passez au contexte approprié avec kubectl :

```sh
kubectl config use-context acgk8s
```

2. Mettre à niveau kubeadm :

```sh
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubeadm=1.27.2-1.1
```

>![Alt text](img/image-1.png)


3. Drainez le nœud du plan de contrôle :

```sh
kubcetl drain acgk8s-control --ignore-daemonsets
```

4. Planifiez la mise à niveau :

```sh
sudo kubeadm upgrade plan v1.27.2
```

>![Alt text](img/image-2.png)


5. Appliquez la mise à niveau :

```sh
sudo kubeadm upgrade apply v1.27.2
```

>![Alt text](img/image-3.png)

6. Mettez à niveau kubelet et kubectl :

```sh
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubelet=1.27.2-1.1 kubectl=1.27.2-1.1
```

ou 

```sh
sudo apt-mark unhold kubelet kubectl && \
sudo apt-get update && \
sudo apt-get install -y kubelet='1.27.2-1.1' kubectl='1.27.2-1.1' && \
sudo apt-mark hold kubelet kubectl
```

>![Alt text](img/image-4.png)

7. Recharger les services systemd :

```sh
sudo systemctl daemon-reload
```

8. Redémarrez kubelet :

```sh
sudo systemctl restart kubelet
```

>![Alt text](img/image-5.png)

9. Débranchez le nœud du plan de contrôle :

```sh
kubectl uncordon acgk8s-control
```

>![Alt text](img/image-6.png)
*Mise à niveau du Control Node réussie*

Ref doc : https://v1-27.docs.kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/#upgrading-control-plane-nodes

## Étape 3 : Mettre à Niveau Tous les Composants Kubernetes sur les Nœuds de Travail

1. Videz le nœud worker1 :

```sh
kubectl drain acgk8s-worker1 --ignore-daemonsets --force
```

2. Connectez-vous en SSH au nœud :

```sh
ssh acgk8s-worker1
```

3. Installez une nouvelle version de kubeadm :

```sh
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubeadm=1.27.2-1.1
```

4. Mettez à niveau le nœud :

```sh
sudo kubeadm upgrade node
```

5. Mettez à niveau kubelet et kubectl :

```sh
sudo apt-get update && \
sudo apt-get install -y --allow-change-held-packages kubelet=1.27.2-1.1 kubectl=1.27.2-1.1
```

6. Recharger les services systemd :

```sh
sudo systemctl daemon-reload
```

7. Redémarrez kubelet :

```sh
sudo systemctl restart kubelet
```

8. Tapez `exit` pour quitter le nœud.

9. Débranchez le nœud :

```sh
kubectl uncordon acgk8s-worker1
```

En faisans un `kubectg get nodes`, l'on peut voir que l'ensemble du cluster est passé de la v1.27.0 à la v1.27.2

>![Alt text](img/image-7.png)

Ref doc : https://v1-27.docs.kubernetes.io/docs/tasks/administer-cluster/kubeadm/upgrading-linux-nodes/#upgrading-worker-nodes

10. Répétez le processus ci-dessus pour `acgk8s-worker2` afin de mettre à niveau l'autre nœud de travail.

En suivant ces étapes, vous aurez mis à niveau avec succès tous les composants Kubernetes sur le nœud du plan de contrôle ainsi que sur les nœuds de travail.